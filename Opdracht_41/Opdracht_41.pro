QT += charts
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
    controlWindow.cpp \
    regelaar.cpp
HEADERS += \
    controlWindow.h \
    regelaar.h
RC_ICONS = iconhot.ico
LIBS += -L$$PWD/./ -lsysteem
