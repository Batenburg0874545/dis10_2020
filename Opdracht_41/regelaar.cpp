#include "regelaar.h"

#define SAMPLETIME 1

void init(void) {
    setIntervalTimer(SAMPLETIME*10);
}


// vraag 4.2 -> 103 = 11001.11 = 25.75
// vraag 4.3 voor 25% => 250
//opdracht 4.4 settings
float phi = 3.0;
float Tsys = 50.0;
float Ksys = 0.67;

float Kc = (1.2 * Tsys)/(Ksys*phi);
float Ti = 2.0 * phi;
float Td = 0.5 * phi;

//snelheidsalgorythme variabelen.
float q0 = Kc*(1+(Td/SAMPLETIME));
float q1 = Kc*(-1+(SAMPLETIME/Ti)-(2*Td/SAMPLETIME));
float q2 = Kc*(Td*SAMPLETIME);

void regel(void)
{
    static float lastOutput = 0;
    static float error[3] = {0, 0, 0};
    float output = 0;

    error[0] = getSetpoint() - (readADC() >> 2); //setpoint - adc;

    output = lastOutput + (q0 * error[0]) + (q1 * error[1]) + (q2 * error[2]);
    if(output >= 1000) output = 1000;
    if(output <= 0) output = 0;

    lastOutput = output;
    for(int i = 2; i >=1; i--)
    {
        error[i] = error[i-1];
    }

    setPWM((uint16_t) output);
}
