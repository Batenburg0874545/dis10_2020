#ifndef HR_BROJZ_REGELAAR_H
#define HR_BROJZ_REGELAAR_H

#include <stdint.h>

// Function 'init' is called once at start of program
void init(void);
// Function 'regel' is called once every interval time:
void regel(void);

// You can use the following functions to implement the control algorithm:
// - set_interval_timer sets the interval time in units of 0.1 s (min = 1, max =100)
void setIntervalTimer(uint8_t intervalTime);
// - readADC reads temperature in degrees Celcius in Q13.2 fixed-point format.
int16_t readADC(void);
// - setPWM sets dutyCycle in 1000 steps (min = 0, max = 1000)
void setPWM(uint16_t dutyCycle);
// - getSetpoint gets setpoint in degrees Celcius (min = 20, max = 100)
uint16_t getSetpoint(void);

#endif // HR_BROJZ_REGELAAR_H
