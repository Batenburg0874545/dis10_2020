#include <stdint.h>
#include <process.h>
#include "fdacoefs_ldf_1K_Q0_16.h"

/*
 * Opdracht 2.1C en 2.2
 *
 *
 * Gemaakt door: Stephan Batenburg
 */

#define N BL-1

int16_t process_left_sample(int16_t sample)
{
    //Uitvoering van 2.1C
        static int16_t buffer[BL];
        int32_t output = 0;
        buffer[0] = sample;

        for(int k = 0; k <= N; k++)
        {
            output = output + (Bint[k] * buffer[k]);
        }

        for(int i = N; i >= 1; i--)
        {
            buffer[i] = buffer[i-1];
        }

        output = output >> 16;
        return (int16_t) output;
}

int16_t process_right_sample(int16_t sample)
{
    //Uitvoering van 2.2
    static int16_t buffer[BL];
    static int circBuffCnt = 0;
    int32_t output = 0;
    buffer[circBuffCnt] = sample;

    int i = circBuffCnt;
    for(int k = 0; k <= N; k++)
    {
        output = output + (Bint[k] * buffer[i]);
        i = i - 1;
        if(i < 0)
        {
            i = N;
        }
    }

    circBuffCnt = (circBuffCnt + 1) % (BL);
    output = output >> 16;
    return (int16_t) output;
}
