#include <stdint.h>
#include <process.h>

/*
 * Opdracht 2.1
 *
 * Gemaakt door: Stephan Batenburg
*/

#define BL 21
#define N BL-1

const double B[BL] = {0.03183098861838 , 0.02500878655992 ,  -9.745429581298e-18,-0.03215415414847 ,
               -0.0530516476973 ,  -0.04501581580786 ,9.745429581298e-18, 0.07502635967976 ,
               0.1591549430919 ,0.2250790790393 , 0.25 , 0.2250790790393 , 0.1591549430919 ,
               0.07502635967976 , 9.745429581298e-18,  -0.04501581580786 ,-0.0530516476973 ,
               -0.03215415414847 ,  -9.745429581298e-18,0.02500878655992 , 0.03183098861838};

const int16_t Bint[21] = {
      981,    780,      0,  -1023,  -1701,  -1453,      0,   2445,   5203,
     7371,   8192,   7371,   5203,   2445,      0,  -1453,  -1701,  -1023,
        0,    780,    981
};

int16_t process_left_sample(int16_t sample)
{
    //uitvoering van 2.1A
    static int16_t buffer[BL];
    int16_t output = 0;
    buffer[0] = sample;

    for(int k = 0; k <= N; k++)
    {
        output = output + (B[k] * buffer[k]);
    }

    for(int i = N; i >= 1; i--)
    {
        buffer[i] = buffer[i-1];
    }

    return output;
}

int16_t process_right_sample(int16_t sample)
{
    //Uitvoering van 2.1B
    static int16_t buffer[BL];
    int32_t output = 0;
    buffer[0] = sample;

    for(int k = 0; k <= N; k++)
    {
        output = output + (Bint[k] * buffer[k]);
    }

    for(int i = N; i >= 1; i--)
    {
        buffer[i] = buffer[i-1];
    }

    output = output >> 15;
    return (int16_t) output;
}
