#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("DIS10 Audio DSP");
    MainWindow w;
    w.setWindowTitle(QCoreApplication::applicationName());
    w.show();
    return a.exec();
}
