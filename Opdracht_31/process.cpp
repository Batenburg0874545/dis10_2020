#include <stdint.h>
#include <process.h>
#include "fdacoefs_opdr3_flt.h"
#include "fdacoefs_opdr3_int.h"

/*
 * Opdracht 3
 *
 *
 * Gemaakt door: Stephan Batenburg
*/

int16_t process_left_sample(int16_t sample)
{
    static int circBuffCntNL = 0;
    static int circBuffCntDL = 0;
    static int16_t inputBuffer[NLint];
    static int32_t outputBuffer[DLint];

    outputBuffer[circBuffCntDL] = 0;
    inputBuffer[circBuffCntNL] = sample;

    int i = circBuffCntNL;
    for(int k = 0; k <= (NLint - 1); k++)
    {
        outputBuffer[circBuffCntDL] = outputBuffer[circBuffCntDL] + (NUMint[k] * inputBuffer[i]);
        i = i - 1;
        if(i < 0)
        {
            i = NLint - 1;
        }
    }

    outputBuffer[circBuffCntDL] = outputBuffer[circBuffCntDL] >> 14;
    i = circBuffCntDL;
    for(int l = 0; l <= (DLint - 1); l++)
    {
        outputBuffer[circBuffCntDL] = outputBuffer[circBuffCntDL] - (DENint[l] * outputBuffer[i]);
        i = i - 1;
        if(i < 0)
        {
            i = DLint - 1;
        }
    }
    outputBuffer[circBuffCntDL] = outputBuffer[circBuffCntDL] >> 14;

    int lastBuffCnt = circBuffCntDL;
    circBuffCntNL = (circBuffCntNL + 1) % (NLint);
    circBuffCntDL = (circBuffCntDL + 1) % (DLint);
    return (int16_t) outputBuffer[lastBuffCnt];
}

int16_t process_right_sample(int16_t sample)
{
    return sample;
}
