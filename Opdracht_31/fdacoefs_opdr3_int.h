/*
 * Filter Coefficients (C Source) generated by the Filter Design and Analysis Tool
 * Generated by MATLAB(R) 9.8 and DSP System Toolbox 9.10.
 * Generated on: 22-Jun-2020 18:37:23
 */

/*
 * Discrete-Time IIR Filter (real)
 * -------------------------------
 * Filter Structure    : Direct-Form I
 * Numerator Length    : 3
 * Denominator Length  : 3
 * Stable              : Yes
 * Linear Phase        : No
 * Arithmetic          : fixed
 * Numerator           : s16,17 -> [-2.500000e-01 2.500000e-01)
 * Denominator         : s16,14 -> [-2 2)
 * Input               : s16,15 -> [-1 1)
 * Output              : s16,10 -> [-32 32)
 * Numerator Prod      : s32,32 -> [-5.000000e-01 5.000000e-01)
 * Denominator Prod    : s32,24 -> [-128 128)
 * Numerator Accum     : s40,32 -> [-128 128)
 * Denominator Accum   : s40,24 -> [-32768 32768)
 * Round Mode          : convergent
 * Overflow Mode       : wrap
 * Cast Before Sum     : true
 */

#include <stdint.h>

const int NLint = 3;
const int16_t NUMint[3] = {
     1600,   3199,   1600
};
const int DLint = 3;
const int16_t DENint[3] = {
    16384, -15447,   5461
};
