#include <stdint.h>
#include <process.h>

// Demo programma DIS10 2020 Hogeschool Rotterdam BroJZ
// Input wordt meteen doorgestuurd naar de output.

int16_t process_left_sample(int16_t sample)
{
    return sample;
}

int16_t process_right_sample(int16_t sample)
{
    return 0;
    //return sample;
}
