#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "process.h"
#include <iostream>
using namespace std;

const int BufferSize = 4096*2;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , audioInput(nullptr)
    , audioOutput(nullptr)
    , input(nullptr)
    , output(nullptr)
    , buffer(BufferSize, 0)
    , process_samples(false)
    , playing(false)
{
    ui->setupUi(this);
    ui->stopButton->setEnabled(false);
    const QAudioDeviceInfo &defaultInputDeviceInfo = QAudioDeviceInfo::defaultInputDevice();
    ui->comboBoxInput->addItem(defaultInputDeviceInfo.deviceName(), QVariant::fromValue(defaultInputDeviceInfo));
    for (auto &deviceInfo: QAudioDeviceInfo::availableDevices(QAudio::AudioInput)) {
        if (deviceInfo != defaultInputDeviceInfo)
            ui->comboBoxInput->addItem(deviceInfo.deviceName(), QVariant::fromValue(deviceInfo));
    }
    const QAudioDeviceInfo &defaultOutputDeviceInfo = QAudioDeviceInfo::defaultOutputDevice();
    ui->comboBoxOutput->addItem(defaultOutputDeviceInfo.deviceName(), QVariant::fromValue(defaultOutputDeviceInfo));
    for (auto &deviceInfo: QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)) {
        if (deviceInfo != defaultOutputDeviceInfo)
            ui->comboBoxOutput->addItem(deviceInfo.deviceName(), QVariant::fromValue(deviceInfo));
    }
    initializeAudioInput(QAudioDeviceInfo::defaultInputDevice());
    initializeAudioOutput(QAudioDeviceInfo::defaultOutputDevice());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initializeAudioInput(const QAudioDeviceInfo& inputDeviceInfo)
{
    format.setSampleRate(sample_rate); // Set frequency to 44100
    format.setChannelCount(2); // Set channels to stereo
    format.setSampleSize(16); // Set sample size to 16 bit
    format.setSampleType(QAudioFormat::SignedInt ); // Sample type as signed integer sample
    format.setByteOrder(QAudioFormat::LittleEndian); // Byte order
    format.setCodec("audio/pcm"); // Set codec as simple audio/pcm

    if (!inputDeviceInfo.isFormatSupported(format))
    {
        // Default format not supported - trying to use nearest
        format = inputDeviceInfo.nearestFormat(format);
    }
    QString status_message(format.channelCount() == 2 ? "      Stereo " : "      Mono ");
    status_message += "Audio: Fs = " + QString::number(format.sampleRate()) + " Hz";
    ui->statusbar->showMessage(status_message);
    delete audioInput;
    audioInput = new QAudioInput(inputDeviceInfo, format, this);
}

void MainWindow::initializeAudioOutput(const QAudioDeviceInfo& outputDeviceInfo)
{
    format.setSampleRate(sample_rate); // Set frequency to 44100
    format.setChannelCount(2); // Set channels to stereo
    format.setSampleSize(16); // Set sample size to 16 bit
    format.setSampleType(QAudioFormat::SignedInt ); // Sample type as signed integer sample
    format.setByteOrder(QAudioFormat::LittleEndian); // Byte order
    format.setCodec("audio/pcm"); // Set codec as simple audio/pcm

    if (!outputDeviceInfo.isFormatSupported(format))
    {
        // Default format not supported - trying to use nearest
        format = outputDeviceInfo.nearestFormat(format);
    }
    delete audioOutput;
    audioOutput = new QAudioOutput(outputDeviceInfo, format, this);
}

void MainWindow::on_comboBoxInput_activated(int index)
{
    on_stopButton_clicked();
    initializeAudioInput(ui->comboBoxInput->itemData(index).value<QAudioDeviceInfo>());
}

void MainWindow::on_comboBoxOutput_activated(int index)
{
    on_stopButton_clicked();
    initializeAudioOutput(ui->comboBoxOutput->itemData(index).value<QAudioDeviceInfo>());
}

void MainWindow::on_playButton_clicked()
{
    playing = true;
    ui->stopButton->setEnabled(true);
    ui->playButton->setEnabled(false);
    // Audio output device
    output = audioOutput->start();
     // Audio input device
    input = audioInput->start();
    // Connect readyRead signal to readMore slot.
    // Call readMore when audio samples fill in inputbuffer
    connect(input, SIGNAL(readyRead()), SLOT(readMore()));
}

void MainWindow::on_stopButton_clicked()
{
    playing = false;
    ui->stopButton->setEnabled(false);
    ui->playButton->setEnabled(true);
    audioOutput->stop();
    audioInput->stop();
    audioInput->disconnect(this);
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    process_samples = arg1;
}

void MainWindow::readMore()
{
    // Read samples from input
    qint64 len = min(audioInput->bytesReady(), BufferSize);
    qint64 l = input->read(buffer.data(), len);
    if (l > 0)
    {
        if (process_samples)
        {
            // Process samples in buffer as 16-bit signed values
            int16_t* p_sample = reinterpret_cast<int16_t*>(buffer.data());
            for (int i = 0; i < l/4; i++)
            {
                *p_sample = process_left_sample(*p_sample);
                p_sample++;
                *p_sample = process_right_sample(*p_sample);
                p_sample++;
            }
        }
        // Send samples to output
        output->write(buffer.constData(), l);
    }
}

// C-like interface
int get_sample_rate()
{
    return sample_rate;
}

