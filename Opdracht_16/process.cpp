#include <stdint.h>
#include <process.h>

static const int N = 24000;
static const float c = 0.75;

int16_t process_left_sample(int16_t sample)
{
    static int n = 0;
    static int16_t buffer[N];

    int16_t output = sample;
    output = output + (c * buffer[n]);

    buffer[n] = output;

    n = (n+1) % N;

    return output;
}

int16_t process_right_sample(int16_t sample)
{
    static int n = 0;
    static int16_t buffer[N];

    int16_t output = sample;
    output = output + (buffer[n] >> 1);

    buffer[n] = output;

    n = (n+1) % N;

    return output;
}
